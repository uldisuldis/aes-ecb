from functions import readFile, writeFile, genKeys, xor, byteSub, shift, mixCol, arrayToBytes, bytesToArray
import getpass

def main():
    theKey = b'' #hello123456hello
    theInput = b''
    cypherText = b''
    inputString = ""
    while True:
        print("Input file directory")
        inputString = input("Path: ")
        try:
            theInput = readFile(inputString)
            break
        except IOError:
            print("Invalid directory.")
    while True:
        print("Input 128-bit key in hex or ASCII.")
        inputKey = getpass.getpass(prompt='Key: ')
        if len(inputKey) == 16:
            try:
                theKey = inputKey.encode()
                break
            except:
                print("Invalid key.")
                continue
        if len(inputKey) == 32:
            try:
                theKey = bytes.fromhex(inputKey)
                break
            except:
                print("Invalid key.")
                continue
        print("Invalid key lenght.")
    blockNum = len(theInput)//16
    keyArray = [""] * 44
    keyArray[:4] = bytesToArray(theKey)
    genKeys(keyArray)
    for x in range(blockNum):
        if x % 1000 == 0:
            print("Block:", x, "/", blockNum+1)
        cypherText += doSipher(keyArray, theInput[x*16:x*16+16])
    remain = theInput[blockNum*16:]
    remain += bytes([16-len(remain)]) * (16-len(remain))
    cypherText += doSipher(keyArray, remain)
    writeFile(inputString.split('\\')[-1:][0] + ".crypt", cypherText)

def doSipher(keyArray, blockByStr):
    inp = bytesToArray(blockByStr)
    inp = [xor(keyArray[0], inp[0]), xor(keyArray[1], inp[1]), xor(keyArray[2], inp[2]), xor(keyArray[3], inp[3])]
    for rd in range(10):
        sub = [byteSub(inp[0]), byteSub(inp[1]), byteSub(inp[2]), byteSub(inp[3])]
        sif = shift(sub)
        if rd < 9:
            mix = [mixCol(sif[0]), mixCol(sif[1]), mixCol(sif[2]), mixCol(sif[3])]
        else:
            mix = sif
        inp = [xor(keyArray[rd*4+4], mix[0]), xor(keyArray[rd*4+5], mix[1]), xor(keyArray[rd*4+6], mix[2]), xor(keyArray[rd*4+7], mix[3])]
    return arrayToBytes(inp)

main()
