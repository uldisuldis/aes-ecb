from functions import readFile, writeFile, genKeys, xor, byteRes, unShift, unMixC, arrayToBytes, bytesToArray
import getpass

def main():
    theKey = b'' #hello123456hello
    theInput = b''
    plainText = b''
    inputString = ""
    while True:
        print("Input file directory")
        inputString = input("Path: ")
        if inputString[-6:] != ".crypt":
            print("File must have .crypt extension.")
            continue
        try:
            theInput = readFile(inputString)
            break
        except IOError:
            print("Invalid directory.")
    while True:
        print("Input 128-bit key in hex or ASCII.")
        inputKey = getpass.getpass(prompt='Key: ')
        if len(inputKey) == 16:
            try:
                theKey = inputKey.encode()
                break
            except:
                print("Invalid key.")
                continue
        if len(inputKey) == 32:
            try:
                theKey = bytes.fromhex(inputKey)
                break
            except:
                print("Invalid key.")
                continue
        print("Invalid key lenght.")
    blockNum = len(theInput)//16
    keyArray = [""] * 44
    keyArray[:4] = bytesToArray(theKey)
    genKeys(keyArray)
    for x in range(blockNum):
        if x % 1000 == 0:
            print("Block:", x, "/", blockNum)
        plainText += doSipher(keyArray, theInput[x*16:x*16+16])
    trim = ord(plainText[-1:])
    writeFile(inputString[:-6].split('\\')[-1:][0], plainText[:-trim])

def doSipher(keyArray, blockByStr):
    inp = bytesToArray(blockByStr)
    for rd in range(10):
        mix = [xor(keyArray[(9-rd)*4+4], inp[0]), xor(keyArray[(9-rd)*4+5], inp[1]), xor(keyArray[(9-rd)*4+6], inp[2]), xor(keyArray[(9-rd)*4+7], inp[3])]
        if rd > 0:
            sif = [unMixC(mix[0]), unMixC(mix[1]), unMixC(mix[2]), unMixC(mix[3])]
        else:
            sif = mix
        sub = unShift(sif)
        inp = [byteRes(sub[0]), byteRes(sub[1]), byteRes(sub[2]), byteRes(sub[3])]
    inp = [xor(keyArray[0], inp[0]), xor(keyArray[1], inp[1]), xor(keyArray[2], inp[2]), xor(keyArray[3], inp[3])]
    return arrayToBytes(inp)

main()